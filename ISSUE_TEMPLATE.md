## Please note the following tips

* Does the website work after you activate the second switch to filter the HTML source code?
* Is there already an existing issue? (Search for the URL, e.g. "codeberg.org" or "localcdn.de")
* Does the website use a strict SOP? Wiki article: [Broken JavaScript or CSS on some websites](https://codeberg.org/nobody/LocalCDN/wiki/Broken-JavaScript-or-CSS-on-some-websites)

### If none of the points apply
* delete this template and describe the problem
* write the URL in the title so that other users can find this issue as quickly as possible
* one or two screenshots can also be helpful

Thank you for your understanding
